import Validator from './Validator'
import { EMPTY, INVALID, VALID } from './ValidationState'

export default class EmailValidator extends Validator {
  validate (value) {
    if (!value) {
      return EMPTY
    } else if (value.indexOf('@') < 0) {
      return INVALID
    } else {
      return VALID
    }
  }
}
